package ua.kiev.tsv.orders.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.kiev.tsv.orders.abstractTest.AbstractRepositoryTest;
import ua.kiev.tsv.orders.domain.Client;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class ClientRepositoryTest extends AbstractRepositoryTest{
    @Autowired
    private ClientRepository clientRepository;

    private Client testClient;
    private Client testClientForMultipleClientsChecks;

    @Before
    public void setUp() throws Exception {
        tearDown();
        testClient = new Client("John", "Doe", LocalDate.now().minusYears(30), "male", "A1231231213");
        testClientForMultipleClientsChecks = new Client("Jane", "Doe", LocalDate.now().minusYears(30), "female", "B1231231213");
        clientRepository.save(testClient);
        clientRepository.save(testClientForMultipleClientsChecks);
    }

    @After
    public void tearDown() {
        clientRepository.deleteAll();
    }

    @Test
    public void clientShouldBeSaved() throws Exception {
        Client client = clientRepository.save(new Client("name", "lastName", LocalDate.now().minusYears(20), "male", "A1231231213"));

        assertNotNull(client);
        assertNotNull(client.getId());
        assertNotNull(client.getBirthDate());
        assertNotNull(client.getName());
        assertNotNull(client.getLastName());
        assertNotNull(client.getInn());
        assertNotNull(client.getSex());
    }

    @Test
    public void findAllExpectedToReturnAllClients() throws Exception {
        List<Client> clientList = clientRepository.findAll();

        assertFalse(clientList.isEmpty());
        assertEquals(testClient, clientList.get(0));
        assertEquals(testClientForMultipleClientsChecks, clientList.get(1));
    }

    @Test
    public void findByLastNameShouldReturnExistingClients() throws Exception {
        List<Client> clientList = clientRepository.findByLastName(testClient.getLastName());

        assertFalse(clientList.isEmpty());
        assertEquals(testClient, clientList.get(0));
    }

    @Test
    public void clientShouldBeUpdatedAfterChanges() throws Exception {
        testClient.changeName("Petro");

        Client updatedClient = clientRepository.save(testClient);

        assertTrue(clientRepository.getOne(updatedClient.getId()).getName().equals("Petro"));
    }

    @Test
    public void clientShouldBeDeleted() throws Exception {
        clientRepository.delete(testClient);

        assertFalse(clientRepository.exists(testClient.getId()));
    }
}