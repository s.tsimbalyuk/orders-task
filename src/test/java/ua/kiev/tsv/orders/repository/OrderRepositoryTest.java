package ua.kiev.tsv.orders.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.kiev.tsv.orders.abstractTest.AbstractRepositoryTest;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.Order;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class OrderRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientRepository clientRepository;

    private Order testOrder;
    private Order testOrderForMultipleClientsChecks;
    private Client testClient;

    @Before
    public void setUp() throws Exception {
        tearDown();
        testClient = new Client("John", "Doe", LocalDate.now().minusYears(30), "male", "A1231231213");
        clientRepository.save(testClient);
        testOrder = new Order(612L, "New", "UAH", LocalDate.now(), testClient);
        testOrderForMultipleClientsChecks = new Order(21L, "New", "USD", LocalDate.now().minusDays(10), testClient);
        orderRepository.save(testOrder);
        orderRepository.save(testOrderForMultipleClientsChecks);
    }

    @After
    public void tearDown() throws Exception {
        clientRepository.deleteAll();
    }

    @Test
    public void orderShouldBeSaved() throws Exception {
        Order order = orderRepository.save(new Order(212L, "Processing", "UAH", LocalDate.now(), testClient));

        assertNotNull(order);
        assertNotNull(order.getId());
        assertNotNull(order.getStatus());
        assertNotNull(order.getClient());
        assertNotNull(order.getCreationDate());
        assertNotNull(order.getCurrency());
        assertNotNull(order.getSum());
    }

    @Test
    public void findAllExpectedToReturnAllOrders() throws Exception {
        List<Order> orderList = orderRepository.findAll();

        assertFalse(orderList.isEmpty());
        assertEquals(testOrder, orderList.get(0));
        assertEquals(testOrderForMultipleClientsChecks, orderList.get(1));
    }

    @Test
    public void findOrdersByClientIdShouldReturnOrder() throws Exception {
        List<Order> orderList = orderRepository.findOrdersByClientId(testClient.getId());

        assertFalse(orderList.isEmpty());
        assertEquals(testOrder, orderList.get(0));
    }
}