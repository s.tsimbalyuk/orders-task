package ua.kiev.tsv.orders.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import ua.kiev.tsv.orders.abstractTest.AbstractControllerTest;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.ClientDTO;
import ua.kiev.tsv.orders.repository.ClientRepository;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class ClientControllerTest extends AbstractControllerTest {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    /*@LocalServerPort
    private int port;*/

    private Client client;
    private ClientDTO clientDTO;
    private HttpHeaders headers = new HttpHeaders();

    @Before
    public void setUp() throws Exception {
        tearDown();
        headers.set("Authorization", "Basic dGVzdDp0ZXN0");
        clientDTO = new ClientDTO("nameDTO", "lastDTO", LocalDate.now().minusYears(10), "sexDTO", "innDTO");
        client = clientRepository.save(new Client("specific_new_name", "lastName", LocalDate.now(), "male", "inn"));
    }

    @After
    public void tearDown() throws Exception {
        clientRepository.deleteAll();
    }

    @Test
    public void getAllShouldReturnStatusOKAndMatchOurClients() throws Exception {
        ResponseEntity<List<Client>> responseEntity = restTemplate.
                exchange("/client/all", HttpMethod.GET, new HttpEntity<>(null, headers),
                        new ParameterizedTypeReference<List<Client>>() {
                        });
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(client.getName(), responseEntity.getBody().get(0).getName());
        assertEquals(client.getLastName(), responseEntity.getBody().get(0).getLastName());
        assertEquals(client.getBirthDate(), responseEntity.getBody().get(0).getBirthDate());
        assertEquals(client.getInn(), responseEntity.getBody().get(0).getInn());
    }

    @Test
    public void createClient() throws Exception {
        String url = "/client/create";
        Client testClient = new Client(clientDTO);
        HttpEntity<Client> entity = new HttpEntity<>(testClient, headers);
        ResponseEntity<Client> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Client.class);
        Client createdClient = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(clientRepository.findOne(createdClient.getId()).getName(), createdClient.getName());
    }

    @Test
    public void getClient() throws Exception {
        String url = "/client/"+client.getId();
        HttpEntity<Client> entity = new HttpEntity<>(null, headers);
        ResponseEntity<Client> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, Client.class);
        Client receivedClient = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(clientRepository.findOne(client.getId()), receivedClient);
    }
}