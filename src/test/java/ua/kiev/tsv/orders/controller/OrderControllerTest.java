package ua.kiev.tsv.orders.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import ua.kiev.tsv.orders.abstractTest.AbstractControllerTest;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.ClientDTO;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.domain.OrderDTO;
import ua.kiev.tsv.orders.repository.ClientRepository;
import ua.kiev.tsv.orders.repository.OrderRepository;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class OrderControllerTest extends AbstractControllerTest {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    private Order order;
    private Client client;
    private OrderDTO orderDTO;
    private HttpHeaders headers = new HttpHeaders();

    @Before
    public void setUp() throws Exception {
        tearDown();
        headers.set("Authorization", "Basic dGVzdDp0ZXN0");
        client = new Client(new ClientDTO("nameDTO", "lastDTO", LocalDate.now().minusYears(10), "sexDTO", "innDTO"));
        client = clientRepository.save(client);
        order = new Order(10L, "new", "UAH", LocalDate.now());
        order = orderRepository.save(order);
        orderDTO = new OrderDTO(10L, "UAH", LocalDate.now());
    }

    @After
    public void tearDown() throws Exception {
        orderRepository.deleteAll();
        clientRepository.deleteAll();
    }

    @Test
    public void createOrder() throws Exception {
        String url = "/client/"+client.getId()+"/create-order";
        HttpEntity<Order> entity = new HttpEntity<>(order, headers);
        ResponseEntity<Order> responseEntity = restTemplate
                .exchange(url, HttpMethod.POST, entity, Order.class);
        Order createdOrder = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(orderRepository.findOne(order.getId()).getCreationDate(), createdOrder.getCreationDate());
    }

    @Test
    public void getAllOrders() throws Exception {
        ResponseEntity<List<Order>> responseEntity = restTemplate.
                exchange("/order/all", HttpMethod.GET, new HttpEntity<>(null, headers),
                        new ParameterizedTypeReference<List<Order>>() {
                        });
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(order.getCreationDate(), responseEntity.getBody().get(0).getCreationDate());
        assertEquals(order.getStatus(), responseEntity.getBody().get(0).getStatus());
    }

    @Test
    public void updateOrderTest() throws Exception {
        String url = "/client/update-order/"+order.getId();
        orderDTO.setCurrency("NEWCUR");
        HttpEntity<OrderDTO> httpEntity = new HttpEntity<>(orderDTO, headers);
        ResponseEntity<Order> responseEntity = restTemplate
                .exchange(url, HttpMethod.PUT, httpEntity, Order.class, order.getId());
        Order updatedOrder = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotEquals(updatedOrder.getCurrency(), order.getCurrency());
    }
}