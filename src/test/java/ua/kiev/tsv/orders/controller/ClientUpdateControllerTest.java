package ua.kiev.tsv.orders.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import ua.kiev.tsv.orders.abstractTest.AbstractControllerTest;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.repository.ClientRepository;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class ClientUpdateControllerTest extends AbstractControllerTest {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    private Client client;
    private HttpHeaders headers = new HttpHeaders();

    @Before
    public void setUp() throws Exception {
        tearDown();
        headers.set("Authorization", "Basic dGVzdDp0ZXN0");
        client = clientRepository.save(new Client("specific_new_name", "lastName", LocalDate.now(), "male", "inn"));
    }

    @After
    public void tearDown() throws Exception {
        clientRepository.deleteAll();
    }

    @Test
    public void updateClientName() throws Exception {
        String url = "/client/" + client.getId() + "/upd/name";
        String newName = "new_unique_name";
        HttpEntity<String> httpEntity = new HttpEntity<>(newName, headers);
        ResponseEntity<String> responseEntity = restTemplate
                .exchange(url, HttpMethod.PUT, httpEntity, String.class);
        String updatedClientName = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotEquals(updatedClientName, client.getName());
    }

    @Test
    public void updateClientLastName() throws Exception {
        String url = "/client/" + client.getId() + "/upd/lastname";
        String newLastName = "new_unique_last_name";
        HttpEntity<String> httpEntity = new HttpEntity<>(newLastName, headers);
        ResponseEntity<String> responseEntity = restTemplate
                .exchange(url, HttpMethod.PUT, httpEntity, String.class);
        String updatedClientLastName = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotEquals(updatedClientLastName, client.getLastName());
    }

    @Test
    public void updateClientBirthDate() throws Exception {
        String url = "/client/" + client.getId() + "/upd/birthdate";
        String newBirthDate = LocalDate.now().minusYears(33).toString();
        HttpEntity<String> httpEntity = new HttpEntity<>(newBirthDate, headers);
        ResponseEntity<LocalDate> responseEntity = restTemplate
                .exchange(url, HttpMethod.PUT, httpEntity, LocalDate.class);
        LocalDate updatedClientBirthDate = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotEquals(updatedClientBirthDate, client.getBirthDate());
    }

    @Test
    public void updateClientInn() throws Exception {
        String url = "/client/" + client.getId() + "/upd/inn";
        String newInn = "new_inn";
        HttpEntity<String> httpEntity = new HttpEntity<>(newInn, headers);
        ResponseEntity<String> responseEntity = restTemplate
                .exchange(url, HttpMethod.PUT, httpEntity, String.class);
        String updatedInn = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotEquals(updatedInn, client.getInn());
    }

    @Test
    public void updateClientSex() throws Exception {
        String url = "/client/" + client.getId() + "/upd/sex";
        String newSex = "new_sex";
        HttpEntity<String> httpEntity = new HttpEntity<>(newSex, headers);
        ResponseEntity<String> responseEntity = restTemplate
                .exchange(url, HttpMethod.PUT, httpEntity, String.class);
        String updatedSex = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotEquals(updatedSex, client.getInn());
    }

}