package ua.kiev.tsv.orders.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import ua.kiev.tsv.orders.abstractTest.AbstractServiceTest;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.domain.OrderDTO;
import ua.kiev.tsv.orders.repository.ClientRepository;
import ua.kiev.tsv.orders.repository.OrderRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class OrderServiceTest extends AbstractServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    private static Client client;
    private static OrderDTO orderDTO;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        client = new Client("Chance", "Ravinne", LocalDate.now().minusYears(32).minusMonths(7), "male", "F89040139");
        client.setId(1L);
        orderDTO = new OrderDTO(13L, "UAH", LocalDate.now());
    }

    @Test
    public void createOrderShouldSaveNewOrderToRepository() throws Exception {
        Order order = new Order(orderDTO, client);
        orderService.createOrder(order);
        assertEquals(order.getCurrency(), orderDTO.getCurrency());
        assertEquals(order.getCreationDate(), orderDTO.getCreationDate());
    }

    @Test
    public void updateOrderShouldSaveOrderWithNewStatus() throws Exception {
        Order order = new Order(orderDTO, client);
        when(orderRepository.save(order)).thenReturn(order);
        String statusOld = order.getStatus();
        when(orderService.updateStatus(order, "something-fancy")).thenReturn(order);
        String statusNew = order.getStatus();
        assertNotEquals(statusOld, statusNew);
    }

    @Test
    public void getOrder() throws Exception {
        Order order = new Order(orderDTO, client);
        when(orderRepository.save(order)).thenReturn(order);
        assertEquals(orderDTO.getCurrency(), order.getCurrency());
        assertEquals(orderDTO.getSum(), order.getSum());
    }

    @Test
    public void getAllOrdersShouldReturnRealNumberOfOrders() throws Exception {
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order(1L, 1L, "stat", "curr", LocalDate.now(), client));
        orderList.add(new Order(2L, 21L, "new", "UAH", LocalDate.now(), client));
        orderList.add(new Order(3L, 14L, "new", "USD", LocalDate.now(), client));
        when(orderService.getAllOrders()).thenReturn(orderList);

        List<Order> result = orderService.getAllOrders();
        assertEquals(3, result.size());
    }

}