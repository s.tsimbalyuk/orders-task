package ua.kiev.tsv.orders.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.repository.ClientRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ClientServiceTest{

    @Mock
    private static ClientRepository repository;

    @InjectMocks
    private ClientServiceImpl clientService;


    private Client client;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        client = new Client("Chance", "Ravinne", LocalDate.now().minusYears(32).minusMonths(7), "male", "F89040139");
        client.setId(101L);
    }

    @Test
    public void getClientsDetails() throws Exception {
        Client testClient = new Client("Chance", "Ravinne", LocalDate.now().minusYears(32).minusMonths(7), "male", "F89040139");
        testClient.setId(1L);
        when(repository.save(testClient)).thenReturn(testClient);
        assertEquals(testClient.getName(), client.getName());
    }

    @Test
    public void getClientTest() throws Exception {
        Client testClient = new Client("name", "lastName", LocalDate.now(), "male", "inn");
        testClient.setId(1L);
        repository.getOne(testClient.getId());
        verify(repository, atLeastOnce()).getOne(testClient.getId());
    }

    @Test
    public void updateClientNameSupposedToChangeName() throws Exception {
        Client testClient = new Client("name", "lastName", LocalDate.now(), "male", "inn");
        when(repository.save(testClient)).thenReturn(testClient);
        clientService.updateClientName(testClient, "wad");
        assertNotEquals(testClient.getName(), "name");
    }

    @Test
    public void updateClientLastName() throws Exception {
        Client testClient = new Client("name", "lastName", LocalDate.now(), "male", "inn");
        when(repository.save(testClient)).thenReturn(testClient);
        clientService.updateClientLastName(testClient, "wad");
        assertNotEquals(testClient.getName(), "lastName");
    }

    @Test
    public void updateClientBirthDate() throws Exception {
        LocalDate date = LocalDate.of(2014, 11, 22);
        Client testClient = new Client("name", "lastName", date, "male", "inn");
        when(repository.save(testClient)).thenReturn(testClient);
        clientService.updateClientBirthDate(testClient, LocalDate.now());
        assertNotEquals(testClient.getBirthDate(), date);
    }

    @Test
    public void updateClientInn() throws Exception {
        Client testClient = new Client("name", "lastName", LocalDate.now(), "male", "inn");
        when(repository.save(testClient)).thenReturn(testClient);
        clientService.updateClientInn(testClient, "12341");
        assertNotEquals(testClient.getName(), "inn");
    }

    @Test
    public void updateClientSex() throws Exception {
        Client testClient = new Client("name", "lastName", LocalDate.now(), "male", "inn");
        when(repository.save(testClient)).thenReturn(testClient);
        clientService.updateClientSex(testClient, "female");
        assertNotEquals(testClient.getName(), "male");
    }

    @Test
    public void createClientShouldReturnNewClient() throws Exception {
        Client testClient = new Client("name", "lastName", LocalDate.now(), "male", "inn");
        when(clientService.createClient(testClient)).thenReturn(testClient);
        Client savedClient = clientService.createClient(testClient);
        assertEquals("name", savedClient.getName());
        assertEquals(testClient, savedClient);
        assertEquals("male", savedClient.getSex());
        assertEquals("lastName", savedClient.getLastName());
    }

    @Test
    public void clientsList() throws Exception {
        List<Client> clientList = new ArrayList<>();
        clientList.add(new Client("name1", "lastName1", LocalDate.now(), "male", "inn1"));
        clientList.add(new Client("name2", "lastName2", LocalDate.now(), "female", "inn2"));
        clientList.add(new Client("name1", "lastName3", LocalDate.now(), "male", "inn3"));
        when(clientService.getClientsList()).thenReturn(clientList);

        List clientsResultList = clientService.getClientsList();
        assertEquals(3, clientsResultList.size());

    }

}