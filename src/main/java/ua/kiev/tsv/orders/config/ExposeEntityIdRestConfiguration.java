package ua.kiev.tsv.orders.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.Order;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Configuration
public class ExposeEntityIdRestConfiguration extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config
                .exposeIdsFor(Client.class)
                .exposeIdsFor(Order.class)
        ;
    }
}
