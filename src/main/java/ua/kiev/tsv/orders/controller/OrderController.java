package ua.kiev.tsv.orders.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.domain.OrderDTO;
import ua.kiev.tsv.orders.service.OrderService;

import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@RestController
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "/client/{id}/create-order", produces = {"application/json", "application/xml"})
    @ResponseStatus(HttpStatus.CREATED)
    public Order createOrder(@RequestBody OrderDTO orderDTO, @PathVariable("id") Long id) {
        return orderService.createOrder(id, orderDTO);
    }

    @PutMapping(value = "/client/update-order/{orderId}", produces = {"application/json", "application/xml"})
    public Order updateOrder(@RequestBody Order orderDTO,
                             @PathVariable("orderId") Long orderId
                             ) {
        return orderService.updateOrder(orderService.getOrder(orderId), orderDTO.getSum(), orderDTO.getCurrency());
    }

    @PutMapping(value = "/order/update-order-status/{orderId}", produces = {"application/json", "application/xml"})
    public Order updateOrderStatus(@RequestBody String status,
                                   @PathVariable("orderId") Long orderId) {
        return orderService.updateStatus(orderService.getOrder(orderId), status);
    }

    @PutMapping(value = "/order/confirm-order/{orderId}", produces = {"application/json", "application/xml"})
    public Order confirmOrder(@PathVariable("orderId") Long orderId) {
        return orderService.updateStatus(orderService.getOrder(orderId), "confirmed");
    }

    @GetMapping(value = "/order/all", produces = {"application/json", "application/xml"})
    public List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }
}
