package ua.kiev.tsv.orders.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.service.ClientService;

import java.time.LocalDate;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@RestController
public class ClientUpdateController {
    private final ClientService clientService;

    public ClientUpdateController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PutMapping(value = "/client/{id}/upd/name", produces = {"application/json", "application/xml"})
    public String updateClientName(@PathVariable("id") Long id, @RequestBody String name) {
        Client client = clientService.getClient(id);
        clientService.updateClientName(client, name);
        return client.getName();
    }

    @PutMapping(value = "/client/{id}/upd/lastname", produces = {"application/json", "application/xml"})
    public String updateClientLastName(@PathVariable("id") Long id, @RequestBody String lastName) {
        Client client = clientService.getClient(id);
        client = clientService.updateClientLastName(client, lastName);
        return client.getLastName();
    }

    @PutMapping(value = "/client/{id}/upd/birthdate", produces = {"application/json", "application/xml"})
    public LocalDate updateClientBirthDate(@PathVariable("id") Long id, @RequestBody String birthDate) {
        Client client = clientService.getClient(id);
        clientService.updateClientBirthDate(client, LocalDate.parse(birthDate));
        return client.getBirthDate();
    }

    @PutMapping(value = "/client/{id}/upd/inn", produces = {"application/json", "application/xml"})
    public String updateClientInn(@PathVariable("id") Long id, @RequestBody String inn) {
        Client client = clientService.getClient(id);
        clientService.updateClientInn(client, inn);
        return client.getInn();
    }

    @PutMapping(value = "/client/{id}/upd/sex", produces = {"application/json", "application/xml"})
    public String updateClientSex(@PathVariable("id") Long id, @RequestBody String sex) {
        Client client = clientService.getClient(id);
        clientService.updateClientSex(client, sex);
        return client.getSex();
    }
}
