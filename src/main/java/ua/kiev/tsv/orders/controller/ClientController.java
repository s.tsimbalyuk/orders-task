package ua.kiev.tsv.orders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.ClientDTO;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.repository.ClientDetails;
import ua.kiev.tsv.orders.service.ClientService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@RestController
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping(value = "/client/create", produces = {"application/json", "application/xml"})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Client createClient(@RequestBody ClientDTO clientDTO) {
        return clientService.createClientFromDTO(clientDTO);
    }

    @GetMapping(value = "/client/{id}", produces = {"application/json", "application/xml"})
    public Client getClient(@PathVariable("id") long id) {
        return clientService.getClient(id);
    }

    @GetMapping(value = "/client/all", produces = {"application/json", "application/xml"})
    @ResponseBody
    public List<Client> getAll() {
        return clientService.getClientsList();
    }

    @GetMapping(value = "/client/{id}/orders/all", produces = {"application/json", "application/xml"})
    @ResponseBody
    public List<Order> getAllOrdersByClientId(@PathVariable("id") long id) {
        return clientService.getClientOrders(id);
    }

    /*no better solution found to run this with projections*/
    @GetMapping(value = "/client/{id}/details", produces = {"application/json", "application/xml"})
    public @ResponseBody
    List<String> getClientDetail(@PathVariable("id") long id) {
        List<ClientDetails> clientDetailsList = clientService.getClientsDetails(id);
        List<String> simple = new ArrayList<>();
        for (ClientDetails clientDetails : clientDetailsList) {
            simple.add(clientDetails.toString());
        }
        return simple;
    }
}
