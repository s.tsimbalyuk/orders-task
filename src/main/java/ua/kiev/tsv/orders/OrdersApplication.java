package ua.kiev.tsv.orders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@SpringBootApplication
public class OrdersApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrdersApplication.class, args);
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /*
    *
    * CORS Filter
    * https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
    *
    * */
    @Component
//    @Order(Ordered.HIGHEST_PRECEDENCE)
    class CORSFilter implements Filter {
        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

            HttpServletResponse response = (HttpServletResponse) res;
            HttpServletRequest request = (HttpServletRequest) req;

            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, DELETE");
            response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            response.setHeader("Access-Control-Max-Age", "4800");

            if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                chain.doFilter(request, response);
            }
        }

        public void init(FilterConfig filterConfig) {
        }

        public void destroy() {
        }
    }

    @Configuration
    @Profile("dev")
    public class DevConfig implements CommandLineRunner {

        public static final String NEW_YEAR = "2018-01-01";

        @Override
        public void run(String... strings) throws Exception {
            if (!LocalDate.parse(NEW_YEAR).equals(LocalDate.now())) {
                System.out.println("Not a new year yet");
            } else {
                System.out.println("OMG! It's too late to party.\nYour initial db population for testing purposes could be here.\nFor more information contact our manager 555-55-55");
            }
        }
    }
}
