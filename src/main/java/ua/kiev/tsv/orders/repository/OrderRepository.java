package ua.kiev.tsv.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ua.kiev.tsv.orders.domain.Order;

import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@RepositoryRestResource
public interface OrderRepository extends JpaRepository <Order, Long> {
    List<Order> findOrdersByClientId(Long id);
}
