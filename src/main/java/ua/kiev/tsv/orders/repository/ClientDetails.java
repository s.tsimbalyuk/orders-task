package ua.kiev.tsv.orders.repository;

import java.time.LocalDate;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */

/*
* using projections for filtering
* https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#projections
* */
public interface ClientDetails {
    String getName();
    String getLastName();
    String getSex();
    String getInn();
    LocalDate getBirthDate();
}
