package ua.kiev.tsv.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ua.kiev.tsv.orders.domain.Client;

import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Repository
public interface ClientRepository extends JpaRepository <Client, Long> {
    List<ClientDetails> findProjectedById(Long id);
    List<Client> findByLastName(String lastName);
}
