package ua.kiev.tsv.orders.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Entity
@Table(name = "client_orders")
public class Order {
    @Id
    @GeneratedValue
    private Long id;
    private Long sum;
    private String status;
    private String currency;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate creationDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private Client client;

    public Order() {
    }


    public Order(OrderDTO orderDTO, Client client) {
        this.sum = orderDTO.getSum();
        this.status = "not confirmed";
        this.currency = orderDTO.getCurrency();
        this.creationDate = LocalDate.now();
        this.client = client;
    }

    public Order(Long id, Long sum, String status, String currency, LocalDate creationDate, Client client) {
        this.id = id;
        this.sum = sum;
        this.status = status;
        this.currency = currency;
        this.creationDate = creationDate;
        this.client = client;
    }

    public Order(Long sum, String status, String currency, LocalDate creationDate, Client client) {
        this.sum = sum;
        this.status = status;
        this.currency = currency;
        this.creationDate = creationDate;
        this.client = client;
    }

    public Order(Long sum, String status, String currency, LocalDate creationDate) {
        this.sum = sum;
        this.status = status;
        this.currency = currency;
        this.creationDate = creationDate;
    }

    /*public void markProcessed() {
        if (orderStatus.equals(OrderStatus.PROCESSING)) {
            this.orderStatus = OrderStatus.PROCESSED;
        } else {
            throw new RuntimeException("Order wasn't processed");
        }
    }

    public void markFailed() {
        if (orderStatus.equals(OrderStatus.PROCESSING)) {
            this.orderStatus = OrderStatus.ERROR;
        } else {
            throw new RuntimeException("Failed to fail. Well, this happens");
        }
    }

    public void markReSent() {
        if (orderStatus.equals(OrderStatus.ERROR)) {
            this.orderStatus = OrderStatus.NEW;
        } else {
            throw new RuntimeException("Failed to reset message status to NEW");
        }
    }

    public void markProcessing() {
        this.orderStatus = OrderStatus.PROCESSING;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return id != null ? id.equals(order.id) : order.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", sum=" + sum +
                ", status='" + status + '\'' +
                ", currency='" + currency + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
