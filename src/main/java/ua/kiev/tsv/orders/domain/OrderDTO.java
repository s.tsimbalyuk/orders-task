package ua.kiev.tsv.orders.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class OrderDTO {
    private Long sum;
    private String currency;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate creationDate;


    public OrderDTO() {
    }

    public OrderDTO(Long sum, String currency, LocalDate creationDate) {
        this.sum = sum;
        this.currency = currency;
        this.creationDate = creationDate;
    }

    public OrderDTO(Long sum, String currency) {
        this.sum = sum;
        this.currency = currency;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = LocalDate.now();
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }
}
