package ua.kiev.tsv.orders.clr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.repository.ClientRepository;
import ua.kiev.tsv.orders.repository.OrderRepository;
import ua.kiev.tsv.orders.service.ClientService;

import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Component
public class DummyData implements CommandLineRunner {
    private final ClientRepository clientRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public DummyData(ClientRepository clientRepository, OrderRepository orderRepository) {
        this.clientRepository = clientRepository;
        this.orderRepository = orderRepository;
    }

    /*used just to populate db on first start with dummy data*/
    //TODO:uncomment
    @Override
    public void run(String... args) throws Exception {
        /*Client clientOne = new Client("John", "Doe", LocalDate.now().minusYears(30), "male", "A1231231213");
        Client clientTwo = new Client("Anveri", "Bestigo", LocalDate.now().minusYears(22).minusMonths(2), "female", "B42352324");
        Client clientThree = new Client("Chance", "Ravinne", LocalDate.now().minusYears(32).minusMonths(7), "male", "F89040139");
        Client clientFour = new Client("Aldent", "Arkanon", LocalDate.now().minusYears(51).minusMonths(1), "male", "Z19271422");
        Stream.of(clientOne, clientTwo, clientThree, clientFour)
                .forEach(clientRepository::save);
        Order order = new Order(12L, "Pending", "UAH", LocalDate.now(), clientTwo);
        Order orderTwo = new Order(13L, "Rejected", "USD", LocalDate.now(), clientOne);
        Order orderThree = new Order(612L, "New", "UAH", LocalDate.now(), clientFour);
        Order orderFour = new Order(319L, "Error", "UAH", LocalDate.now(), clientThree);
        Stream.of(order, orderTwo, orderThree, orderFour)
                .forEach(orderRepository::save);
        clientRepository.findAll().forEach(System.out::println);
        orderRepository.findAll().forEach(System.out::println);*/
    }
}
