package ua.kiev.tsv.orders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.ClientDTO;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.repository.ClientDetails;
import ua.kiev.tsv.orders.repository.ClientRepository;
import ua.kiev.tsv.orders.repository.OrderRepository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Service
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository, OrderRepository orderRepository) {
        this.clientRepository = clientRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public Client createClient(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Client createClientFromDTO(ClientDTO clientDTO) {
        return clientRepository.save(new Client(clientDTO));
    }

    @Override
    public List<Client> getClientsList() {
        return clientRepository.findAll();
    }

    @Override
    public List<ClientDetails> getClientsDetails(Long id) {
        return clientRepository.findProjectedById(id);
    }

    @Override
    public Client getClient(Long id) {
        return clientRepository.findOne(id);
    }

    @Override
    public Client updateClientName(Client client, String name) {
        client.changeName(name);
        return clientRepository.save(client);
    }

    @Override
    public Client updateClientLastName(Client client, String lastName) {
        client.changeLastName(lastName);
        return clientRepository.save(client);
    }

    @Override
    public Client updateClientBirthDate(Client client, LocalDate birthDate) {
        client.changeBirthDate(birthDate);
        return clientRepository.save(client);
    }

    @Override
    public Client updateClientInn(Client client, String inn) {
        client.setInn(inn);
        return clientRepository.save(client);
    }

    @Override
    public Client updateClientSex(Client client, String sex) {
        client.setSex(sex);
        return clientRepository.save(client);
    }

    @Override
    public List<Order> getClientOrders(Long id) {
        return orderRepository.findOrdersByClientId(id);
    }
}
