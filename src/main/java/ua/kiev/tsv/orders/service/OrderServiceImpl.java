package ua.kiev.tsv.orders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.domain.OrderDTO;
import ua.kiev.tsv.orders.repository.ClientRepository;
import ua.kiev.tsv.orders.repository.OrderRepository;

import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, ClientRepository clientRepository) {
        this.orderRepository = orderRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public Order createOrder(Long id, OrderDTO orderDTO) {
        Order order = new Order(orderDTO, clientRepository.findOne(id));
        return orderRepository.save(order);
    }

    @Override
    public Order updateOrder(Order order, Long sum, String currency) {
        order.setSum(sum);
        order.setCurrency(currency);
        return orderRepository.save(order);
    }

    @Override
    public Order updateStatus(Order order, String status) {
        order.setStatus(status);
        return orderRepository.save(order);
    }


    @Override
    public Order getOrder(Long id) {
        return orderRepository.findOne(id);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }
}
