package ua.kiev.tsv.orders.service;

import ua.kiev.tsv.orders.domain.Client;
import ua.kiev.tsv.orders.domain.ClientDTO;
import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.repository.ClientDetails;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public interface ClientService {
    List<Client> getClientsList();

    List<ClientDetails> getClientsDetails(Long id);

    Client getClient(Long id);

    Client updateClientName(Client client, String name);

    Client updateClientLastName(Client client, String lastName);

    Client updateClientBirthDate(Client client, LocalDate birthDate);

    Client updateClientInn(Client client, String inn);

    Client updateClientSex(Client client, String sex);

    Client createClient(Client client);

    Client createClientFromDTO(ClientDTO clientDTO);

    List<Order> getClientOrders(Long id);
}
