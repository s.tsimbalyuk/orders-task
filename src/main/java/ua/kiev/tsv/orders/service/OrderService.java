package ua.kiev.tsv.orders.service;

import ua.kiev.tsv.orders.domain.Order;
import ua.kiev.tsv.orders.domain.OrderDTO;

import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public interface OrderService {
    Order createOrder(Long id, OrderDTO order);

    Order updateOrder(Order order, Long sum, String currency);

    Order updateStatus(Order order, String status);

    Order getOrder(Long id);

    public List<Order> getAllOrders();

    Order createOrder(Order order);
}
